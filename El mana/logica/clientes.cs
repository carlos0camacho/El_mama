using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using datos;
using System.Windows.Forms;


namespace Logica {
    public class CLIENTES
    {

        public SqlCommand comando;
        public DataSet ds;
        public SqlDataAdapter da;

        private int cod_Cliente;
        private string nombres;
        private string apellido;
        private string f_nacimiento;
        private char sexo;
        private string direccion;
        private int telefono;

        public int Cod_Cliente
        {
            get { return cod_Cliente; }
            set { cod_Cliente = value; }
        }

        public string Nombres
        {
            get { return nombres; }
            set { nombres = value; }
        }

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public string F_nacimiento
        {
            get { return f_nacimiento; }
            set { f_nacimiento = value; }
        }

        public char Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }

        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public int Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public void ingresar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec insertarCLIENTES {0},'{1}','{2}','{3}','{4}','{5}',{6}", cod_Cliente, nombres,apellido, f_nacimiento, sexo, direccion, telefono);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos registrados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }



        public void actualizar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec actualizarCLIENTES {0},'{1}','{2}','{3}','{4}','{5}',{6}", cod_Cliente, nombres, apellido, f_nacimiento, sexo, direccion, telefono);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de" + cod_Cliente + "Actualizados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public void eliminar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec eliminarCLIENTES {0}", cod_Cliente);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de" + cod_Cliente + "Eliminados");
                }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public DataSet consultar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                da = new SqlDataAdapter("select*from clientesVw", con.conexion);
                ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return ds = new DataSet();
            }
        }






    }//end CLIENTES
}
