using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using datos;
using System.Windows.Forms;


namespace Logica
{
    public class EMPLEADOS
    {
        private int cod_Empleado;
        private string nombres;
        private string apellidos;
        private string contraseņa;
        private char sexo;
        private string f_nacimiento;
        private string eps;
        private double salario;




        public SqlCommand comando;
        public DataSet ds;
        public SqlDataAdapter da;

        //Metodos accesores
        public int Cod_Empleado
        {
            get { return cod_Empleado; }
            set { cod_Empleado = value; }
        }
        public string Nombres
        {
            get { return nombres; }
            set { nombres = value; }
        }
        public string Apellidos
        {
            get { return apellidos; }
            set { apellidos = value; }
        }
        public string Contraseņa
        {
            get { return contraseņa; }
            set { contraseņa = value; }
        }
        public char Sexo
        {
            get { return sexo; }
            set { sexo = value; }
        }
        public string F_nacimiento
        {
            get { return f_nacimiento; }
            set { f_nacimiento = value; }
        }
        public string Eps
        {
            get { return eps; }
            set { eps = value; }
        }
            public double Salario
        {
            get { return salario; }
            set { salario = value; }
        }

        public void ingresar() {
            Conexion con=new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText= string.Format("exec insertarEMPLEADOS {0},'{1}','{2}','{3}','{4}','{5}','{6}',{7} ", cod_Empleado,nombres,apellidos,contraseņa,sexo,f_nacimiento,eps,salario );
                comando.Connection= con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos registrados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error"+ ex.Message.ToString()); }
        }



        public void actualizar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec actualizarEMPLEADOS {0},'{1}','{2}','{3}','{4}','{5}','{6}',{7} ", cod_Empleado, nombres, apellidos, contraseņa, sexo, f_nacimiento, eps, salario);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de"+ Cod_Empleado +"Actualizados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public void eliminar() {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec eliminarEMPLEADOS {0}", cod_Empleado);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de" + Cod_Empleado + "Eliminados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public DataSet consultar() {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                da = new SqlDataAdapter("select*from EmpleadosVw", con.conexion);
                ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return ds = new DataSet();
            }
        }



    }
}


