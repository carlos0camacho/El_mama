using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using datos;
using System.Windows.Forms;
namespace Logica {
    public class DETALLEVENTAPRODUCTOS {

        private int cod_D_Venta_PRODUCTO;
        private int cod_Venta;
        private int cantidad_Productos;
        private int cod_Producto;
        public SqlCommand comando;
        public DataSet ds;
        public SqlDataAdapter da;

        public int Cod_D_Venta_PRODUCTO
        {
            get { return cod_D_Venta_PRODUCTO; }
            set { cod_D_Venta_PRODUCTO = value; }
        }
        public int Cod_Venta
        {
            get { return cod_Venta; }
            set { cod_Venta = value; }
        }
        public int Cantidad_Productos
        {
            get { return cantidad_Productos; }
            set { cantidad_Productos = value; }
        }
        public int Cod_Producto
        {
            get { return cod_Producto; }
            set { cod_Producto = value; }
        }


    }
}