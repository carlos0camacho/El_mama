using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using datos;
using System.Windows.Forms;

namespace Logica
{
    public class SERVICIOS
    {

        private int cod_Servicio;
        private string nombre;
        private string descripcion;
        private double valor;

        public int Cod_Servicio
        {
            get { return cod_Servicio; }
            set { cod_Servicio = value; }
        }


        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }

        public double Valor
        {
            get { return valor; }
            set { valor = value; }
        }

        public SqlCommand comando;
        public DataSet ds;
        public SqlDataAdapter da;

        public void ingresar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec insertarSERVICIOS {0},'{1}','{2}',{3}", cod_Servicio, nombre, descripcion, valor);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos registrados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }



        public void actualizar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec actualizarSERVICIOS {0},'{1}','{2}',{3}", cod_Servicio, nombre, descripcion, valor);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de" + cod_Servicio + "Actualizados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public void eliminar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec eliminarSERVICIOS {0}", cod_Servicio);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de" + cod_Servicio + "Eliminados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public DataSet consultar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                da = new SqlDataAdapter("select*from serviciosVw", con.conexion);
                ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return ds = new DataSet();
            }
        }

    }
}