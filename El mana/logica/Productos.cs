using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using datos;
using System.Windows.Forms;
namespace Logica
{
    public class PRODUCTOS
    {
        public SqlCommand comando;
        public DataSet ds;
        public SqlDataAdapter da;

        private int cod_producto;
        private string nombre_Producto;
        private double valorUnidad;
        private int cantidad_Disponible;


        public int Cod_producto
        {
            get { return cod_producto; }
            set { cod_producto = value; }
        }
      

        public string Nombre_Producto
        {
            get { return nombre_Producto; }
            set { nombre_Producto = value; }
        }


        public int Cantidad_Disponible
        {
            get { return cantidad_Disponible; }
            set { cantidad_Disponible = value; }
        }


        public double ValorUnidad
        {
            get { return valorUnidad; }
            set { valorUnidad = value; }
        }

        public void ingresar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec insertarPRODUCTOS {0},'{1}',{2},{3}", cod_producto, nombre_Producto, valorUnidad, cantidad_Disponible);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos registrados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }



        public void actualizar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec actualizarPRODUCTOS {0},'{1}',{2},{3}", cod_producto, nombre_Producto, valorUnidad, cantidad_Disponible);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de" + cod_producto + "Actualizados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public void eliminar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                comando = new SqlCommand();
                comando.CommandText = string.Format("exec eliminarPRODUCTOS {0}", cod_producto);
                comando.Connection = con.conexion;
                comando.ExecuteNonQuery();
                MessageBox.Show("Datos de" + cod_producto + "Eliminados");
            }
            catch (Exception ex)
            { MessageBox.Show("se ha producido un error" + ex.Message.ToString()); }
        }
        public DataSet consultar()
        {
            Conexion con = new datos.Conexion();
            con.abrir();
            try
            {
                da = new SqlDataAdapter("select*from productosVw", con.conexion);
                ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                return ds = new DataSet();
            }
        }




    }
}

