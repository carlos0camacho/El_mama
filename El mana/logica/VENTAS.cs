using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using datos;
using System.Windows.Forms;

namespace Logica
{
    public class VENTAS
    {
        public SqlCommand comando;
        public DataSet ds;
        public SqlDataAdapter da;

        private int cod_Venta;
        private string fecha_Venta;
        private int cod_Empleado;
        private int cod_Cliente;


        public int Cod_Venta
        {
            get { return cod_Venta; }
            set { cod_Venta = value; }
        }
        public string Fecha_Venta
        {
            get { return fecha_Venta; }
            set { fecha_Venta = value; }
        }
        public int Cod_Empleado
        {
            get { return cod_Empleado; }
            set { cod_Empleado = value; }
        }
        public int Cod_Cliente
        {
            get { return cod_Cliente; }
            set { cod_Cliente = value; }
        }

 




    }
}