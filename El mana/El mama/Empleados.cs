﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace elmama
{
    public partial class Empleados : Form
    {
        Logica.EMPLEADOS emp;
        public Empleados()
        {
            InitializeComponent();
        }

        private void Empleados_Load(object sender, EventArgs e)
        {

        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            emp = new Logica.EMPLEADOS();
            try
            {
                emp.Cod_Empleado = int.Parse(textBox1.Text);
                emp.Nombres = textBox2.Text;
                emp.Apellidos = textBox3.Text;
                emp.Contraseña = textBox4.Text;
                emp.F_nacimiento = dateTimePicker1.Value.Day + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Year;
                emp.Eps = textBox7.Text;
                emp.Salario = double.Parse(textBox8.Text);
                emp.Sexo = textBox5.Text[0];
                emp.ingresar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }

        private void Empleados_Load_1(object sender, EventArgs e)
        {

        }

        private void Btn_Buscar_Click(object sender, EventArgs e)
        {
            emp=new Logica.EMPLEADOS();
            dataGridView1.DataSource = emp.consultar().Tables[0];
        }

        private void Btn_Actualizar_Click(object sender, EventArgs e)
        {
            try
            {
                emp.Cod_Empleado = int.Parse(textBox1.Text);
                emp.Nombres = textBox2.Text;
                emp.Apellidos = textBox3.Text;
                emp.Contraseña = textBox4.Text;
                emp.F_nacimiento = dateTimePicker1.Value.Day + "/" + dateTimePicker1.Value.Month + "/" + dateTimePicker1.Value.Year;
                emp.Eps = textBox7.Text;
                emp.Salario = double.Parse(textBox8.Text);
                emp.Sexo = textBox5.Text[0];
                emp.actualizar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            try
            {
                emp.Cod_Empleado = int.Parse(textBox1.Text);
                emp.eliminar();
            }
            catch { MessageBox.Show("Verifique los campos"); }

        }



    }
}
