﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace elmama
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        private void Principal_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Empleados emp = new Empleados();
            emp.MdiParent = this;
            emp.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
           Clientes cli = new Clientes();
           cli.MdiParent = this;
           cli.Show();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Productos pro= new Productos();
            pro.MdiParent = this;
            pro.Show();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Servicios ser = new Servicios();
            ser.MdiParent = this;
            ser.Show();
        }
    }
}
