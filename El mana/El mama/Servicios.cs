﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace elmama
{
    public partial class Servicios : Form
    {
        public Servicios()
        {
            InitializeComponent();
        }
        Logica.SERVICIOS ser; 
        private void Productos_Load(object sender, EventArgs e)
        {

        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            ser = new Logica.SERVICIOS();
           try
            {
                ser.Cod_Servicio = int.Parse(textBox1.Text);
                ser.Nombre = textBox2.Text;
                ser.Descripcion = textBox3.Text;
                ser.Valor = double.Parse(textBox4.Text);
                ser.ingresar();
            }
            catch { MessageBox.Show("Verifique los campos"); }

        }

        private void Btn_Buscar_Click(object sender, EventArgs e)
        {
            ser = new Logica.SERVICIOS();
            dataGridView1.DataSource = ser.consultar().Tables[0];
        }

        private void Btn_Actualizar_Click(object sender, EventArgs e)
        {
            ser = new Logica.SERVICIOS();
            try
            {
                ser.Cod_Servicio = int.Parse(textBox1.Text);
                ser.Nombre = textBox2.Text;
                ser.Descripcion = textBox3.Text;
                ser.Valor = double.Parse(textBox4.Text);
                ser.actualizar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            ser = new Logica.SERVICIOS();
            try
            {
                ser.Cod_Servicio = int.Parse(textBox1.Text);
                ser.eliminar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }
    }
}
