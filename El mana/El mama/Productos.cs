﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace elmama
{
    public partial class Productos : Form
    {
        public Productos()
        {
            InitializeComponent();
        }
        Logica.PRODUCTOS pro;


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            pro = new Logica.PRODUCTOS();
            try
            {
                pro.Cod_producto = int.Parse(textBox1.Text);
                pro.Nombre_Producto = textBox2.Text;
                pro.Cantidad_Disponible = int.Parse(textBox3.Text);
                pro.ValorUnidad = double.Parse(textBox4.Text);
                pro.ingresar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }

        private void Btn_Buscar_Click(object sender, EventArgs e)
        {
            pro= new Logica.PRODUCTOS();
            dataGridView1.DataSource = pro.consultar().Tables[0];

        }

        private void Btn_Actualizar_Click(object sender, EventArgs e)
        {

            pro = new Logica.PRODUCTOS();
            try
            {
                pro.Cod_producto = int.Parse(textBox1.Text);
                pro.Nombre_Producto = textBox2.Text;
                pro.Cantidad_Disponible = int.Parse(textBox3.Text);
                pro.ValorUnidad = double.Parse(textBox4.Text);
                pro.actualizar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
            
        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            pro = new Logica.PRODUCTOS();
            try
            {
                pro.Cod_producto = int.Parse(textBox1.Text);
                pro.eliminar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }
    }
}
