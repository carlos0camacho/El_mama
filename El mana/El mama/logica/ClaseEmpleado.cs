﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using datos;

namespace logica
{
   public class ClaseEmpleado
    {
        //atributos
        private int codigo;
        private string nombre;
        private string apellido;
        private string fecha_nacimiento;
        private string cargo;
        private string contraseña;
        private string direccion;
        private int telefono;
        public DataSet ds;
        public SqlDataAdapter da;

        //propiedades

        public int CODIGO
        {
            get { return codigo; }
            set { codigo = value; }
        }
        public string NOMBRE
        {
            get { return nombre; }
            set { nombre = value; }
        }
        public string APELLIDO
        {
            get { return apellido; }
            set { apellido = value; }
        }
        public string FECHA_NACIMIENTO
        {
            get { return fecha_nacimiento; }
            set { fecha_nacimiento = value; }
        }
        public string CARGO
        {
            get { return cargo; }
            set { cargo = value; }
        }
        public string CONTRASEÑA
        {
            get { return contraseña; }
            set { contraseña = value; }
        }
        public string DIRECCION
        {
            get { return direccion; }
            set { direccion = value; }
        }
        public int TELEFONO
        {
            get { return telefono; }
            set { telefono = value; }
        }
        //Metodos

        public string logeo()
        {
            datos.conexion connet = new datos.conexion();
            string perfil;
            da = new SqlDataAdapter(string.Format("exec logeo {0},{1}", codigo, contraseña),connet.Conexion);
            ds = new DataSet();
            da.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                perfil = ds.Tables[0].Rows[0][2].ToString();
                }
            else
            {
                perfil="";
            }
            return perfil;
            }
        }

    }


