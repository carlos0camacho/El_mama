﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace elmama
{
    public partial class Clientes : Form
    {
        Logica.CLIENTES cli;
        public Clientes()
        {
            InitializeComponent();
        }

        private void Btn_Guardar_Click(object sender, EventArgs e)
        {
            cli = new Logica.CLIENTES();
            try
            {
                cli.Cod_Cliente=int.Parse(textBox1.Text);
                cli.Nombres = textBox2.Text;
                cli.Apellido = textBox3.Text;
                cli.F_nacimiento = textBox4.Text;
                cli.Sexo = (textBox5.Text)[0];
                cli.Direccion = textBox6.Text;
                cli.Telefono = int.Parse(textBox7.Text);
                cli.ingresar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }

        private void Btn_Buscar_Click(object sender, EventArgs e)
        {
            cli=new Logica.CLIENTES();
            dataGridView1.DataSource = cli.consultar().Tables[0];
        }

        private void Btn_Actualizar_Click(object sender, EventArgs e)
        {
            cli = new Logica.CLIENTES();
            try
            {
                cli.Cod_Cliente = int.Parse(textBox1.Text);
                cli.Nombres = textBox2.Text;
                cli.Apellido = textBox3.Text;
                cli.F_nacimiento = textBox4.Text;
                cli.Sexo = (textBox5.Text)[0];
                cli.Direccion = textBox6.Text;
                cli.Telefono = int.Parse(textBox7.Text);
                cli.actualizar();
            }
            catch { MessageBox.Show("Verifique los campos"); }

        }

        private void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            cli = new Logica.CLIENTES();
            try
            {
                cli.Cod_Cliente = int.Parse(textBox1.Text);
                cli.eliminar();
            }
            catch { MessageBox.Show("Verifique los campos"); }
        }
    }
}
